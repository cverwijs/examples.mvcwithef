$mstestpath = 'C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\Common7\IDE\MSTest.exe'
$coveragedir = '.\CodeCoverage'

if(!(Test-Path -Path $coveragedir )){
    New-Item -ItemType directory -Path $coveragedir
}

&".\packages\OpenCover.4.6.519\tools\OpenCover.Console.exe" `
-register:user `
-target:$mstestpath `
-targetargs:"/noresults /noisolation /testcontainer:"".\Website.Tests\bin\debug\Website.Tests.dll""  /testcontainer:"".\Repositories.Tests\bin\debug\Repositories.Tests.dll""" `
-skipautoprops `
-filter:"+[*]* -[Website.Tests]* -[Repositories.Tests]*" `
-excludebyattribute:"*.GeneratedCodeAttribute*;*.ExcludeFromCodeCoverage*" `
-hideskipped:All `
-output:$coveragedir\coverage.xml

.\packages\ReportGenerator.2.5.8\tools\ReportGenerator.exe -verbosity:Error -reporttypes:"Badges;Html;HtmlChart;PngChart" -reports:"$coveragedir\coverage.xml" -targetdir:"$coveragedir"