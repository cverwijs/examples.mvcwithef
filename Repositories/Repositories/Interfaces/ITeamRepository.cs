﻿using System;
using System.Collections.Generic;

namespace Repositories
{
    public interface ITeamRepository : IRepositoryBase<Team>
    {
    }
}
