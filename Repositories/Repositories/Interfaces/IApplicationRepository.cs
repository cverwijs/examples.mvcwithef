﻿using System;
using System.Collections.Generic;

namespace Repositories
{
    public interface IApplicationRepository : IRepositoryBase<Application>
    {
    }
}
