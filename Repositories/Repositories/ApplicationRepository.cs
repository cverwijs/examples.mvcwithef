﻿using System.Diagnostics.CodeAnalysis;

namespace Repositories
{
    [ExcludeFromCodeCoverage] 
    public class ApplicationRepository : RepositoryBase<Application>, IApplicationRepository
    {
        public ApplicationRepository(IDataContext dataContext)
            : base(dataContext)
        {
        }
    }
}
